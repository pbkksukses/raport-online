package com.mycompany.raport.data.dao;

import com.mycompany.raport.domain.KMPT;

import java.util.List;
import java.util.UUID;

public interface KMPT_Dao {
    public List<KMPT> list();
    public KMPT_Dao findByID(UUID id);
    public void save(KMPT data);
    public void delete(KMPT data);
    public List<KMPT> findByName(String name);
}
