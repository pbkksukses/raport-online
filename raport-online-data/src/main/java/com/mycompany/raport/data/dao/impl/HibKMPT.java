package com.mycompany.raport.data.dao.impl;

import com.mycompany.raport.data.dao.KMPT_Dao;
import com.mycompany.raport.domain.KMPT;
import java.util.List;
import java.util.UUID;
import javax.annotation.Resource;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("KmptDao")
public class HibKMPT implements KMPT_Dao{

    private SessionFactory sessionFactory;
    
    public HibKMPT(){}
    
    @Resource(name="sessionFactory")
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    
    public SessionFactory getSessionFactory(){
        return sessionFactory;
    }
    
    @Transactional(readOnly = true)
    public List<KMPT> list() {
        List<KMPT> data = this.getSessionFactory().getCurrentSession()
                .createQuery("from KMPT").list();
        return data;
    }

    public KMPT_Dao findByID(UUID id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional
    public void save(KMPT data) {
        Session session = this.sessionFactory.openSession();
        //Transaction tx = this.getSessionFactory().openSession().beginTransaction();
        //this.getSessionFactory().getCurrentSession().save(data);
        Transaction tx = session.beginTransaction();
        session.persist(data);
        //session.flush();
        tx.commit();
        session.close();
    }

    public void delete(KMPT data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<KMPT> findByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
