package com.mycompany.raport.data.console;

import java.util.List;
import com.mycompany.raport.domain.KMPT;
import com.mycompany.raport.data.dao.KMPT_Dao;

import org.springframework.context.support.GenericXmlApplicationContext;
public class ConsoleKMPT {

    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:datasource.xml");
        ctx.refresh();
        KMPT data_1 = 
               new KMPT("Pengembangan Keterampilan siswa",2,"Siswa diajak untuk mengembangkan dirinya ke arah yang positif");
        KMPT 
                data_2 = new KMPT("Pengembangan Kejiwaan siswa",6,"Siswa diajak untuk mengembangkan kejiwaan dirinya ke arah yang positif");
        KMPT 
                data_3 = new KMPT("Pengembangan Kepribadian siswa",9,"Siswa dituntut untuk dapat menerapkan kepribadian baiknya");
        
        KMPT_Dao kompetensiMataPelajaranTambahanDao = 
                ctx.getBean("KmptDao",KMPT_Dao.class);
        kompetensiMataPelajaranTambahanDao.save(data_1);
        kompetensiMataPelajaranTambahanDao.save(data_2);
        kompetensiMataPelajaranTambahanDao.save(data_3);
        
        /*List<KMPT> data = kompetensiMataPelajaranTambahanDao.list();
		for (KMPT c : data) {
			System.out.println(c);
		}*/
        System.out.println("Silahkan Lihat database PostgreSQL anda");
    }
    
}
