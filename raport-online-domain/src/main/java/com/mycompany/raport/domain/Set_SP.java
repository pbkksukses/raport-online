package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="set_sp")
public class Set_SP {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name = "ID_Setting")
    private UUID ID_Setting;
    
    @Column(name="Nomor_Pokok_Sekolah_Nasional")
    private int Nomor_Pokok_Sekolah_Nasional;
    
    @Column(name = "Status_Sekolah")
    private String Status_Sekolah;
    
    @Column(name = "Alamat")
    private String Alamat;
    
    @Column(name = "Website")
    private String Website;
    
    @Column(name = "ID_Jenjang_Pendidikan")
    private UUID ID_Jenjang_Pendidikan;
    
    
    public Set_SP(int Nomor_Pokok_Sekolah_Nasional,String Status_Sekolah,String Alamat,String Website,UUID ID_Jenjang_Pendidikan){
        this.setNomor_Pokok_Sekolah_Nasional(Nomor_Pokok_Sekolah_Nasional);
        this.setAlamat(Alamat);
        this.setID_Jenjang_Pendidikan(ID_Jenjang_Pendidikan);
        this.setNomor_Pokok_Sekolah_Nasional(Nomor_Pokok_Sekolah_Nasional);
        this.setStatus_Sekolah(Status_Sekolah);
        this.setWebsite(Website);
    }

    public int getNomor_Pokok_Sekolah_Nasional() {
        return Nomor_Pokok_Sekolah_Nasional;
    }

    public void setNomor_Pokok_Sekolah_Nasional(int Nomor_Pokok_Sekolah_Nasional) {
        this.Nomor_Pokok_Sekolah_Nasional = Nomor_Pokok_Sekolah_Nasional;
    }

    public String getStatus_Sekolah() {
        return Status_Sekolah;
    }

    public void setStatus_Sekolah(String Status_Sekolah) {
        this.Status_Sekolah = Status_Sekolah;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String Alamat) {
        this.Alamat = Alamat;
    }

    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String Website) {
        this.Website = Website;
    }

    public UUID getID_Jenjang_Pendidikan() {
        return ID_Jenjang_Pendidikan;
    }

    public void setID_Jenjang_Pendidikan(UUID ID_Jenjang_Pendidikan) {
        this.ID_Jenjang_Pendidikan = ID_Jenjang_Pendidikan;
    }

    public UUID getID_Setting() {
        return ID_Setting;
    }

    public void setID_Setting(UUID ID_Setting) {
        this.ID_Setting = ID_Setting;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Nomor Pokok Sekolah Nasional :");
        builder.append(Nomor_Pokok_Sekolah_Nasional);
        builder.append("Status Sekolah :");
        builder.append(Status_Sekolah);
        builder.append("Alamat :");
        builder.append(Alamat);
        builder.append("Website :");
        builder.append(Website);
        builder.append("ID Jenjang Pendidikan :");
        builder.append(ID_Jenjang_Pendidikan);
        builder.append("ID Setting :");
        builder.append(ID_Setting);
        return builder.toString();
    }
    
    
}
