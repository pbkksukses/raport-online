package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="kmp")
public class KMP {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_KMPT")
    private UUID ID_KMPT;
    
    @Column(name="Jenis_Nilai_KMP")
    private String Jenis_Nilai_KMP;
    
    @Column(name="Nomor_KMP")
    private int Nomor_KMP;
    
    @Column(name="Des_KMP")
    private String Des_KMP;
    
    @Column(name="ID_Mata_Pelajaran")
    private UUID ID_Mata_Pelajaran;
    
    public KMP(String Jenis_Nilai_KMP,int Nomor_KMP,String Des_KMP){
        this.setJenis_Nilai_KMP(Jenis_Nilai_KMP);
        this.setNomor_KMP(Nomor_KMP);
        this.setDes_KMP(Des_KMP);
    }

    public UUID getID_KMPT() {
        return ID_KMPT;
    }

    public void setID_KMPT(UUID ID_KMPT) {
        this.ID_KMPT = ID_KMPT;
    }

    public String getJenis_Nilai_KMP() {
        return Jenis_Nilai_KMP;
    }

    public void setJenis_Nilai_KMP(String Jenis_Nilai_KMP) {
        this.Jenis_Nilai_KMP = Jenis_Nilai_KMP;
    }

    public int getNomor_KMP() {
        return Nomor_KMP;
    }

    public void setNomor_KMP(int Nomor_KMP) {
        this.Nomor_KMP = Nomor_KMP;
    }

    public String getDes_KMP() {
        return Des_KMP;
    }

    public void setDes_KMP(String Des_KMP) {
        this.Des_KMP = Des_KMP;
    }

    public UUID getID_Mata_Pelajaran() {
        return ID_Mata_Pelajaran;
    }

    public void setID_Mata_Pelajaran(UUID ID_Mata_Pelajaran) {
        this.ID_Mata_Pelajaran = ID_Mata_Pelajaran;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID KMP :");
        builder.append(ID_KMPT);
        builder.append("Jenis Nilai KMP :");
        builder.append(Jenis_Nilai_KMP);
        builder.append("Nomor KMP :");
        builder.append(Nomor_KMP);
        builder.append("ID Mata Pelajaran :");
        builder.append(ID_Mata_Pelajaran);    
        return builder.toString();
    }  
    }
