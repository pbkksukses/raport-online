package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="nilai_sikap")
public class Nilai_Sikap {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Nilai_Sikap")
    private UUID ID_Nilai_Sikap;
    
    @Column(name = "Jurnal")
    private String Jurnal;
    
    @Column(name = "Nilai_Observasi_Pendidik")
    private char Nilai_Observasi_Pendidik;
    
    @Column(name = "Penilaian_Diri")
    private char Penilaian_Diri;
    
    @Column(name = "ID_Nilai")
    private UUID ID_Nilai;
    
    public  Nilai_Sikap(String Jurnal,char Nilai_Observasi_Pendidik,char Nilai_Pendidik,UUID ID_Nilai){
        this.setJurnal(Jurnal);
        this.setNilai_Observasi_Pendidik(Nilai_Observasi_Pendidik);
        this.setPenilaian_Pendidik(Penilaian_Diri);
        this.setID_Nilai(ID_Nilai);
    }

    public UUID getID_Nilai_Sikap() {
        return ID_Nilai_Sikap;
    }

    public void setID_Nilai_Sikap(UUID ID_Nilai_Sikap) {
        this.ID_Nilai_Sikap = ID_Nilai_Sikap;
    }

    public String getJurnal() {
        return Jurnal;
    }

    public void setJurnal(String Jurnal) {
        this.Jurnal = Jurnal;
    }

    public char getNilai_Observasi_Pendidik() {
        return Nilai_Observasi_Pendidik;
    }

    public void setNilai_Observasi_Pendidik(char Nilai_Observasi_Pendidik) {
        this.Nilai_Observasi_Pendidik = Nilai_Observasi_Pendidik;
    }

    public char getPenilaian_Pendidik() {
        return Penilaian_Diri;
    }

    public void setPenilaian_Pendidik(char Penilaian_Pendidik) {
        this.Penilaian_Diri = Penilaian_Pendidik;
    }

    public UUID getID_Nilai() {
        return ID_Nilai;
    }

    public void setID_Nilai(UUID ID_Nilai) {
        this.ID_Nilai = ID_Nilai;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Nilai Sikap :");
        builder.append(ID_Nilai_Sikap);
        builder.append("Jurnal :");
        builder.append(Jurnal);
        builder.append("Nilai Observasi Pendidik :");
        builder.append(Nilai_Observasi_Pendidik);
        builder.append("Penilaian Pendidik :");
        builder.append(Penilaian_Diri);
        builder.append("ID Nilai");
        builder.append(ID_Nilai);
        return builder.toString();
    }
}
