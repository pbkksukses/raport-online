package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="nilai_mapel")
public class Nilai_Mapel {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Nilai")
    private UUID ID_Nilai;
    
    @Column(name = "ID_Raport")
    private UUID ID_Raport;
    
    @Column(name = "ID_Mata_Pelajaran_Diajarkan")
    private UUID ID_Mata_Pelajaran_Diajarkan;
    
    @Column(name = "ID_Peserta_Didik")
    private UUID ID_Peserta_Didik;
    
    public Nilai_Mapel(UUID ID_Raport,UUID Mata_Pelajaran_Diajarkan,UUID ID_peseta_Didik){
        this.setID_Raport(ID_Raport);
        this.setID_Mata_Pelajaran_Diajarkan(ID_Mata_Pelajaran_Diajarkan);
        this.setID_Peserta_Didik(ID_Peserta_Didik);
    }

    public UUID getID_Nilai() {
        return ID_Nilai;
    }

    public void setID_Nilai(UUID ID_Nilai) {
        this.ID_Nilai = ID_Nilai;
    }

    public UUID getID_Raport() {
        return ID_Raport;
    }

    public void setID_Raport(UUID ID_Raport) {
        this.ID_Raport = ID_Raport;
    }

    public UUID getID_Mata_Pelajaran_Diajarkan() {
        return ID_Mata_Pelajaran_Diajarkan;
    }

    public void setID_Mata_Pelajaran_Diajarkan(UUID ID_Mata_Pelajaran_Diajarkan) {
        this.ID_Mata_Pelajaran_Diajarkan = ID_Mata_Pelajaran_Diajarkan;
    }

    public UUID getID_Peserta_Didik() {
        return ID_Peserta_Didik;
    }

    public void setID_Peserta_Didik(UUID ID_Peserta_Didik) {
        this.ID_Peserta_Didik = ID_Peserta_Didik;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Nilai :");
        builder.append(ID_Nilai);
        builder.append("ID Raport :");
        builder.append(ID_Raport);
        builder.append("ID mata Pelajaran Diajarkan :");
        builder.append(ID_Mata_Pelajaran_Diajarkan);
        builder.append("ID Peserta Didik :");
        builder.append(ID_Peserta_Didik);
        return builder.toString();
    }
}
