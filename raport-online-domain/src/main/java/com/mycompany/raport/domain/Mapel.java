package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="mapel")
public class Mapel {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Mapel")
    private UUID ID_Mapel;
    
    @Column(name = "Nama_Mapel")
    private String Nama_Mapel;
    
    @Column(name = "Deskripsi_Mapel")
    private String Deskripsi_Mapel;
    
    @Column(name="ID_Tnkt_Pendi")
    private UUID ID_Tnkt_Pendi;
    
    public Mapel(String Nama_Mapel,String Deskripsi_Mapel,UUID ID_Tnkt_Pendi){
        this.setNama_Mapel(Nama_Mapel);
        this.setDeskripsi_Mapel(Deskripsi_Mapel);
        this.setID_Tnkt_Pendi(ID_Tnkt_Pendi);
    }

    public UUID getID_Mapel() {
        return ID_Mapel;
    }

    public void setID_Mapel(UUID ID_Mapel) {
        this.ID_Mapel = ID_Mapel;
    }

    public String getNama_Mapel() {
        return Nama_Mapel;
    }

    public void setNama_Mapel(String Nama_Mapel) {
        this.Nama_Mapel = Nama_Mapel;
    }

    public String getDeskripsi_Mapel() {
        return Deskripsi_Mapel;
    }

    public void setDeskripsi_Mapel(String Deskripsi_Mapel) {
        this.Deskripsi_Mapel = Deskripsi_Mapel;
    }

    public UUID getID_Tnkt_Pendi() {
        return ID_Tnkt_Pendi;
    }

    public void setID_Tnkt_Pendi(UUID ID_Tnkt_Pendi) {
        this.ID_Tnkt_Pendi = ID_Tnkt_Pendi;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Mapel :");
        builder.append(ID_Mapel);
        builder.append("Nama Mapel :");
        builder.append(Nama_Mapel);
        builder.append("Deskripsi Mapel :");
        builder.append(Deskripsi_Mapel);
        builder.append("ID Tingkat Pendidikan :");
        builder.append(ID_Tnkt_Pendi);
        return builder.toString();
    }
}
