package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="peserta_didik")
public class Peserta_Didik {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Peserta_Didik")
    private UUID ID_Peserta_Didik;
    
    @Column(name = "NISN")
    private String NISN;
    
    @Column(name = "Nama_Peserta_Didik")
    private String Nama_Peserta_Didik;
    
    public Peserta_Didik(String NISN,String Nama_Peserta_Didik){
        this.setNISN(NISN);
        this.setNama_Peserta_Didik(Nama_Peserta_Didik);
    }

    public UUID getID_Peserta_Didik() {
        return ID_Peserta_Didik;
    }

    public void setID_Peserta_Didik(UUID ID_Peserta_Didik) {
        this.ID_Peserta_Didik = ID_Peserta_Didik;
    }

    public String getNISN() {
        return NISN;
    }

    public void setNISN(String NISN) {
        this.NISN = NISN;
    }

    public String getNama_Peserta_Didik() {
        return Nama_Peserta_Didik;
    }

    public void setNama_Peserta_Didik(String Nama_Peserta_Didik) {
        this.Nama_Peserta_Didik = Nama_Peserta_Didik;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Peserta Didik :");
        builder.append(ID_Peserta_Didik);
        builder.append("NISN :");
        builder.append(NISN);
        builder.append("Nama Peserta Didik :");
        builder.append(Nama_Peserta_Didik);
        return builder.toString();
    }
}
