package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="rombel")
public class RomBel {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Rombel")
    private UUID ID_Rombel;
    
    @Column(name = "Nama_Rombel")
    private String Nama_Rombel;
    
    @Column(name = "ID_PDTK")
    private UUID ID_PDTK;
    
    @Column(name = "ID_Tingkat_Pendidikan")
    private UUID ID_Tingkat_Pendidikan;
    
    public RomBel(String Nama_Rombel,UUID ID_PDTK,UUID ID_Tingkat_Pendidikan){
        this.setNama_Rombel(Nama_Rombel);
        this.setID_PDTK(ID_PDTK);
        this.setID_Tingkat_Pendidikan(ID_Tingkat_Pendidikan);
    }

    public UUID getID_Rombel() {
        return ID_Rombel;
    }

    public void setID_Rombel(UUID ID_Rombel) {
        this.ID_Rombel = ID_Rombel;
    }

    public String getNama_Rombel() {
        return Nama_Rombel;
    }

    public void setNama_Rombel(String Nama_Rombel) {
        this.Nama_Rombel = Nama_Rombel;
    }

    public UUID getID_PDTK() {
        return ID_PDTK;
    }

    public void setID_PDTK(UUID ID_PDTK) {
        this.ID_PDTK = ID_PDTK;
    }

    public UUID getID_Tingkat_Pendidikan() {
        return ID_Tingkat_Pendidikan;
    }

    public void setID_Tingkat_Pendidikan(UUID ID_Tingkat_Pendidikan) {
        this.ID_Tingkat_Pendidikan = ID_Tingkat_Pendidikan;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Rombongan Belajar :");
        builder.append(ID_Rombel);
        builder.append("Nama Rombongan Belajar :");
        builder.append(Nama_Rombel);
        builder.append("ID PDTK:");
        builder.append(ID_PDTK);
        builder.append("ID Tingkat Pendidikan :");
        builder.append(ID_Tingkat_Pendidikan);
        return builder.toString();
    }
}
