package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="jenj_pendi")
public class Jenj_Pendi {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Jenj_Pendi")
    private UUID ID_Jenj_Pendi;
    
    @Column(name = "Nma_Jenj_Pendi")
    private String Nma_Jenj_Pendi;
    
    @Column(name = "Levl_Jenj_Pendi")
    private int Levl_Jenj_Pendi;
    
    public Jenj_Pendi(String Nma_Jenj_Pendi,int Levl_Jenj_Pendi){
        this.setNma_Jenj_Pendi(Nma_Jenj_Pendi);
        this.setLevl_Jenj_Pendi(Levl_Jenj_Pendi);
    }

    public UUID getID_Jenj_Pendi() {
        return ID_Jenj_Pendi;
    }

    public void setID_Jenj_Pendi(UUID ID_Jenj_Pendi) {
        this.ID_Jenj_Pendi = ID_Jenj_Pendi;
    }

    public String getNma_Jenj_Pendi() {
        return Nma_Jenj_Pendi;
    }

    public void setNma_Jenj_Pendi(String Nma_Jenj_Pendi) {
        this.Nma_Jenj_Pendi = Nma_Jenj_Pendi;
    }

    public int getLevl_Jenj_Pendi() {
        return Levl_Jenj_Pendi;
    }

    public void setLevl_Jenj_Pendi(int Levl_Jenj_Pendi) {
        this.Levl_Jenj_Pendi = Levl_Jenj_Pendi;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Jenjang Pendidikan :");
        builder.append(ID_Jenj_Pendi);
        builder.append("Nama Jenjang Pendidikan :");
        builder.append(Nma_Jenj_Pendi);
        builder.append("Level Jenjang Pendidikan :");
        builder.append(Levl_Jenj_Pendi);
        return builder.toString();
    }
}
