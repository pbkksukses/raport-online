package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="jenis_pdtk")
public class Jenis_PDTK {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Jenis_PDTK")
    private UUID ID_Jenis_PDTK;
    
    @Column(name = "Nama_Jenis_PDTK")
    private String Nama_Jenis_PDTK;
    
    public Jenis_PDTK(String Nama_Jenis_PDTK){
        this.setNama_Jenis_PDTK(Nama_Jenis_PDTK);
    }

    public UUID getID_Jenis_PDTK() {
        return ID_Jenis_PDTK;
    }

    public void setID_Jenis_PDTK(UUID ID_Jenis_PDTK) {
        this.ID_Jenis_PDTK = ID_Jenis_PDTK;
    }

    public String getNama_Jenis_PDTK() {
        return Nama_Jenis_PDTK;
    }

    public void setNama_Jenis_PDTK(String Nama_Jenis_PDTK) {
        this.Nama_Jenis_PDTK = Nama_Jenis_PDTK;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID PDTK :");
        builder.append(ID_Jenis_PDTK);
        builder.append("Nama PDTK :");
        builder.append(Nama_Jenis_PDTK);
        return builder.toString();
    }
}
