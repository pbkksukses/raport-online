package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="tgktpendi")
public class TgktPendi {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Tingkat_Pendidikan")
    private UUID ID_Tingkat_Pendidikan;
    
    @Column(name="Nama_Tingkat_Pendidikan")
    private String Nama_Tingkat_Pendidikan;
    
    public TgktPendi(String Nama_Tingkat_Pendidikan){
        this.setNama_Tingkat_Pendidikan(Nama_Tingkat_Pendidikan);
    }

    public UUID getID_Tingkat_Pendidikan() {
        return ID_Tingkat_Pendidikan;
    }

    public void setID_Tingkat_Pendidikan(UUID ID_Tingkat_Pendidikan) {
        this.ID_Tingkat_Pendidikan = ID_Tingkat_Pendidikan;
    }

    public String getNama_Tingkat_Pendidikan() {
        return Nama_Tingkat_Pendidikan;
    }

    public void setNama_Tingkat_Pendidikan(String Nama_Tingkat_Pendidikan) {
        this.Nama_Tingkat_Pendidikan = Nama_Tingkat_Pendidikan;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Tingkat Pendidikan :");
        builder.append(ID_Tingkat_Pendidikan);
        builder.append("Nama Tingkat Pendidikan :");
        builder.append(Nama_Tingkat_Pendidikan);
        return builder.toString();
    }
    
    
}
