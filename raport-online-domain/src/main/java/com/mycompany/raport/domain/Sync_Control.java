package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="sync_control")
public class Sync_Control {
    @Column(name = "Soft_Delete")
    private boolean Soft_Delete;
    
    @Column(name = "Create_date")
    private String Create_Date;
    
    @Column(name = "Last_Update")
    private String Last_Update;
    
    public Sync_Control(Boolean Soft_Delete,String create_Date,String Last_Update){
        this.setCreate_Date(Create_Date);
        this.setLast_Update(Last_Update);
        this.setSoft_Delete(Soft_Delete);
    }

    public boolean isSoft_Delete() {
        return Soft_Delete;
    }

    public void setSoft_Delete(boolean Soft_Delete) {
        this.Soft_Delete = Soft_Delete;
    }

    public String getCreate_Date() {
        return Create_Date;
    }

    public void setCreate_Date(String Create_Date) {
        this.Create_Date = Create_Date;
    }

    public String getLast_Update() {
        return Last_Update;
    }

    public void setLast_Update(String Last_Update) {
        this.Last_Update = Last_Update;
    }
}
