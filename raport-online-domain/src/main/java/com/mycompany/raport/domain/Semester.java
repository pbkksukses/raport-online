package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="semester")
public class Semester {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Semester")
    private UUID ID_Semester;
    
    @Column(name = "Nama_Semester")
    private String Nama_Semester;
    
    @Column(name = "ID_Tahun_Ajaran")
    private UUID ID_Tahun_Ajaran;
    
    public Semester(String Nama_Semester,UUID ID_Tahun_Ajaran){
        this.setNama_Semester(Nama_Semester);
        this.setID_Tahun_Ajaran(ID_Tahun_Ajaran);
    }

    public UUID getID_Semester() {
        return ID_Semester;
    }

    public void setID_Semester(UUID ID_Semester) {
        this.ID_Semester = ID_Semester;
    }

    public String getNama_Semester() {
        return Nama_Semester;
    }

    public void setNama_Semester(String Nama_Semester) {
        this.Nama_Semester = Nama_Semester;
    }

    public UUID getID_Tahun_Ajaran() {
        return ID_Tahun_Ajaran;
    }

    public void setID_Tahun_Ajaran(UUID ID_Tahun_Ajaran) {
        this.ID_Tahun_Ajaran = ID_Tahun_Ajaran;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Semester :");
        builder.append(ID_Semester);
        builder.append("Nama Semester :");
        builder.append(Nama_Semester);
        builder.append("ID tahun Ajaran :");
        builder.append(ID_Tahun_Ajaran);
        return builder.toString();
    }
    
    
}
