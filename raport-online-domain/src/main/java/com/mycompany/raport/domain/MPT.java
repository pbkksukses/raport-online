package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="mpt")
public class MPT {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_KMPT")
    private UUID ID_KMPT;
    
    @Column(name = "des_MPT")
    private String des_MPT;
    
    @Column(name = "jenis_nilai_KMPT")
    private String jenis_nilai_KMPT;
    
    @Column(name = "nomor_KMPT")
    private int nomor_KMPT;
            
    public MPT(String des_MPT,String jenis_nilai_KMPT,int nomor_KMPT){
        this.setDes_MPT(des_MPT);
        this.setJenis_nilai_KMPT(jenis_nilai_KMPT);
        this.setNomor_KMPT(nomor_KMPT);
    }

    public UUID getID_MPT() {
        return ID_KMPT;
    }

    public void setID_MPT(UUID ID_MPT) {
        this.ID_KMPT = ID_MPT;
    }

    public String getDes_MPT() {
        return des_MPT;
    }

    public void setDes_MPT(String des_MPT) {
        this.des_MPT = des_MPT;
    }

    public String getJenis_nilai_KMPT() {
        return jenis_nilai_KMPT;
    }

    public void setJenis_nilai_KMPT(String jenis_nilai_KMPT) {
        this.jenis_nilai_KMPT = jenis_nilai_KMPT;
    }

    public int getNomor_KMPT() {
        return nomor_KMPT;
    }

    public void setNomor_KMPT(int nomor_KMPT) {
        this.nomor_KMPT = nomor_KMPT;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID MPT :");
        builder.append(ID_KMPT);
        builder.append("Deskripsi KMPT :");
        builder.append(des_MPT);
        builder.append("Deskripsi KMPT :");
        builder.append(des_MPT);
        builder.append("Jenis KMPT :");
        builder.append(jenis_nilai_KMPT);
        builder.append("Nomor KMPT :");
        builder.append(nomor_KMPT);
        return builder.toString();
    }
}
