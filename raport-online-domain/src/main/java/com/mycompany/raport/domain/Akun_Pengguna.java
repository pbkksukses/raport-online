package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="akun_pengguna")
public class Akun_Pengguna {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Pengguna")
    private UUID ID_Pengguna;
    
    @Column(name = "Username")
    private String Username;
    
    @Column(name = "Password")
    private String Password;
    
    @Column(name = "Apakah_Akun_Aktif")
    private Boolean Apakah_Akun_Aktif;
    
    @Column(name = "ID_PDTK")
    private UUID ID_PDTK;
    
    public Akun_Pengguna(String Username,String Password,Boolean Apakah_Akun_Aktif,UUID ID_PDTK){
        this.setUsername(Username);
        this.setPassword(Password);
        this.setApakah_Akun_Aktif(Apakah_Akun_Aktif);
        this.setID_PDTK(ID_PDTK);
    }

    public UUID getID_Pengguna() {
        return ID_Pengguna;
    }

    public void setID_Pengguna(UUID ID_Pengguna) {
        this.ID_Pengguna = ID_Pengguna;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public Boolean isApakah_Akun_Aktif() {
        return Apakah_Akun_Aktif;
    }

    public void setApakah_Akun_Aktif(Boolean Apakah_Akun_Aktif) {
        this.Apakah_Akun_Aktif = Apakah_Akun_Aktif;
    }

    public UUID getID_PDTK() {
        return ID_PDTK;
    }

    public void setID_PDTK(UUID ID_PDTK) {
        this.ID_PDTK = ID_PDTK;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Akun Pengguna :");
        builder.append(ID_Pengguna);
        builder.append("Username :");
        builder.append(Username);
        builder.append("Apakah Akun Aktif ? :");
        builder.append(Apakah_Akun_Aktif);
        builder.append("ID PDTK :");
        builder.append(ID_PDTK);
        return builder.toString();
    }
}
