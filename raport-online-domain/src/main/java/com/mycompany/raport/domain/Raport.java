package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="raport")
public class Raport {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Raport")
    private UUID ID_raport;
    
    @Column(name = "Jumlah_Sakit")
    private int Jumlah_sakit;
    
    @Column(name = "Jumlah_Izin")
    private int Jumlah_Izin;
    
    @Column(name = "Jumlah_Tanpa_keterangan")
    private int Jumlah_Tanpa_Keterangan;
    
    @Column(name = "ID_Semester")
    private UUID ID_Semester;
    
    @Column(name = "ID_Rombongan_Belajar")
    private UUID ID_Rombongan_Belajar;
    
    @Column(name = "ID_Peserta_Didik")
    private UUID ID_Peserta_Didik;
    
    public Raport(int Jumlah_Sakit,int Jumlah_Izin,int Jumlah_Tanpa_Keterangan,UUID ID_Semester,UUID ID_Peserta_Didik,UUID ID_Rombongan_Belajar){
    this.setJumlah_sakit(Jumlah_sakit);
    this.setJumlah_Izin(Jumlah_Izin);
    this.setJumlah_Tanpa_Keterangan(Jumlah_Tanpa_Keterangan);
    this.setID_Semester(ID_Semester);
    this.setID_Peserta_Didik(ID_Peserta_Didik);
    this.setID_Rombongan_Belajar(ID_Rombongan_Belajar);
    }

    public UUID getID_raport() {
        return ID_raport;
    }

    public void setID_raport(UUID ID_raport) {
        this.ID_raport = ID_raport;
    }

    public int getJumlah_sakit() {
        return Jumlah_sakit;
    }

    public void setJumlah_sakit(int Jumlah_sakit) {
        this.Jumlah_sakit = Jumlah_sakit;
    }

    public int getJumlah_Izin() {
        return Jumlah_Izin;
    }

    public void setJumlah_Izin(int Jumlah_Izin) {
        this.Jumlah_Izin = Jumlah_Izin;
    }

    public int getJumlah_Tanpa_Keterangan() {
        return Jumlah_Tanpa_Keterangan;
    }

    public void setJumlah_Tanpa_Keterangan(int Jumlah_Tanpa_Keterangan) {
        this.Jumlah_Tanpa_Keterangan = Jumlah_Tanpa_Keterangan;
    }

    public UUID getID_Semester() {
        return ID_Semester;
    }

    public void setID_Semester(UUID ID_Semester) {
        this.ID_Semester = ID_Semester;
    }

    public UUID getID_Rombongan_Belajar() {
        return ID_Rombongan_Belajar;
    }

    public void setID_Rombongan_Belajar(UUID ID_Rombongan_Belajar) {
        this.ID_Rombongan_Belajar = ID_Rombongan_Belajar;
    }

    public UUID getID_Peserta_Didik() {
        return ID_Peserta_Didik;
    }

    public void setID_Peserta_Didik(UUID ID_Peserta_Didik) {
        this.ID_Peserta_Didik = ID_Peserta_Didik;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Raport :");
        builder.append(ID_raport);
        builder.append("Jumlah Sakit :");
        builder.append(Jumlah_sakit);
        builder.append("Jumlah Izin :");
        builder.append(Jumlah_Izin);
        builder.append("Jumlah Tanpa Keterangan :");
        builder.append(Jumlah_Tanpa_Keterangan);
        builder.append("ID Semester :");
        builder.append(ID_Semester);
        builder.append("ID Rombongan Belajar :");
        builder.append(ID_Rombongan_Belajar);
        builder.append("ID Peserta Didik :");
        builder.append(ID_Peserta_Didik);
        return builder.toString();
    }  
}
