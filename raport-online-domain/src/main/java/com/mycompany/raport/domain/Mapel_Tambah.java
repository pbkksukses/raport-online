package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="mapel_tambah")
public class Mapel_Tambah {

    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_MPT")
    private UUID ID_MPT;
    
    @Column(name = "Nama_MPT")
    private String Nama_MPT;
    
    @Column(name = "Jenis_MPT")
    private String Jenis_MPT;
    
    @Column(name = "Deskripsi_MPT")
    private String Deskripsi_MPT;
    
    @Column(name = "ID_Tnkt_Pendi")
    private UUID ID_Tnkt_Pendi;
    
    public Mapel_Tambah(String Nama_MPT,String Jenis_MPT,String Deskripsi_MPT,UUID ID_Tnkt_Pendi){
        this.setNama_MPT(Nama_MPT);
        this.setJenis_MPT(Jenis_MPT);
        this.setDeskripsi_MPT(Deskripsi_MPT);
        this.setID_Tnkt_Pendi(ID_Tnkt_Pendi);
    }

    public UUID getID_MPT() {
        return ID_MPT;
    }

    public void setID_MPT(UUID ID_MPT) {
        this.ID_MPT = ID_MPT;
    }

    public String getNama_MPT() {
        return Nama_MPT;
    }

    public void setNama_MPT(String Nama_MPT) {
        this.Nama_MPT = Nama_MPT;
    }

    public String getJenis_MPT() {
        return Jenis_MPT;
    }

    public void setJenis_MPT(String Jenis_MPT) {
        this.Jenis_MPT = Jenis_MPT;
    }

    public String getDeskripsi_MPT() {
        return Deskripsi_MPT;
    }

    public void setDeskripsi_MPT(String Deskripsi_MPT) {
        this.Deskripsi_MPT = Deskripsi_MPT;
    }

    public UUID getID_Tnkt_Pendi() {
        return ID_Tnkt_Pendi;
    }

    public void setID_Tnkt_Pendi(UUID ID_Tnkt_Pendi) {
        this.ID_Tnkt_Pendi = ID_Tnkt_Pendi;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Mapel Tambah :");
        builder.append(ID_MPT);
        builder.append("Nama Mapel Tambah :");
        builder.append(Nama_MPT);
        builder.append("Jenis Mapel Tambah :");
        builder.append(Jenis_MPT);
        builder.append("Deskripsi Mapel Tambah :");
        builder.append(Deskripsi_MPT);
        builder.append("ID Tnkt Pendidikan :");
        builder.append(ID_Tnkt_Pendi);
        return builder.toString();
    }
}
