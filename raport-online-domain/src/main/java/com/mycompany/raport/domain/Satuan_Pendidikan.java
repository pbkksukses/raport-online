package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="satuan_pendidikan")
public class Satuan_Pendidikan {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Satuan_Pendidikan")
    private UUID ID_Satuan_Pendidikan;
    
    @Column(name = "Nama_Satuan_Pendidikan")
    private String Nama_Satuan_Pendidikan;
    
    @Column(name = "ID_Setting")
    private UUID ID_Setting;
    
    public Satuan_Pendidikan(String Nama_Satuan_Pendidikan,UUID ID_Setting){
        this.setNama_Satuan_Pendidikan(Nama_Satuan_Pendidikan);
        this.setID_Setting(ID_Setting);
    }

    public UUID getID_Satuan_Pendidikan() {
        return ID_Satuan_Pendidikan;
    }

    public void setID_Satuan_Pendidikan(UUID ID_Satuan_Pendidikan) {
        this.ID_Satuan_Pendidikan = ID_Satuan_Pendidikan;
    }

    public String getNama_Satuan_Pendidikan() {
        return Nama_Satuan_Pendidikan;
    }

    public void setNama_Satuan_Pendidikan(String Nama_Satuan_Pendidikan) {
        this.Nama_Satuan_Pendidikan = Nama_Satuan_Pendidikan;
    }

    public UUID getID_Setting() {
        return ID_Setting;
    }

    public void setID_Setting(UUID ID_Setting) {
        this.ID_Setting = ID_Setting;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Satuan Pendidikan :");
        builder.append(ID_Satuan_Pendidikan);
        builder.append("Nama Satuan Pendidikan :");
        builder.append(Nama_Satuan_Pendidikan);
        builder.append("ID Setting :");
        builder.append(ID_Setting);
        return builder.toString();
    } 
}
