package com.mycompany.raport.domain;


import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="pdtk")
public class pdtk {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_PDTK")
    private UUID ID_PDTK;
    
    @Column(name = "NUPTK")
    private String NUPTK;
    
    @Column(name = "Nama_PDTK")
    private String Nama_PDTK;
    
    @Column(name = "ID_Jenis_PDTK")
    private UUID ID_Jenis_PDTK;
    
    public pdtk(String NUPDTK,String Nama_PDTK,UUID ID_Jenis_PDTK){
        this.setNUPTK(NUPTK);
        this.setNama_PDTK(Nama_PDTK);
        this.setID_Jenis_PDTK(ID_Jenis_PDTK);
    }

    public UUID getID_PDTK() {
        return ID_PDTK;
    }

    public void setID_PDTK(UUID ID_PDTK) {
        this.ID_PDTK = ID_PDTK;
    }

    public String getNUPTK() {
        return NUPTK;
    }

    public void setNUPTK(String NUPTK) {
        this.NUPTK = NUPTK;
    }

    public String getNama_PDTK() {
        return Nama_PDTK;
    }

    public void setNama_PDTK(String Nama_PDTK) {
        this.Nama_PDTK = Nama_PDTK;
    }

    public UUID getID_Jenis_PDTK() {
        return ID_Jenis_PDTK;
    }

    public void setID_Jenis_PDTK(UUID ID_Jenis_PDTK) {
        this.ID_Jenis_PDTK = ID_Jenis_PDTK;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID PDTK :");
        builder.append(ID_PDTK);
        builder.append("NUPTK :");
        builder.append(NUPTK);
        builder.append("Nama PDTK :");
        builder.append(Nama_PDTK);
        builder.append("ID Jenis PDTK :");
        builder.append(ID_Jenis_PDTK);
        return builder.toString();
    }
}
