package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="angrombel")
public class AngRombel {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_AngRombel")
    private UUID ID_AngRombel;
    
    @Column(name = "ID_Peserta_Didik")
    private UUID ID_Peserta_Didik;
    
    @Column(name = "ID_RomBel")
    private UUID ID_RomBel;
    
    public AngRombel(UUID ID_Peserta_Didik,UUID ID_Rombel){
        this.setID_Peserta_Didik(ID_Peserta_Didik);
        this.setID_RomBel(ID_RomBel);
    }

    public UUID getID_AngRombel() {
        return ID_AngRombel;
    }

    public void setID_AngRombel(UUID ID_AngRombel) {
        this.ID_AngRombel = ID_AngRombel;
    }

    public UUID getID_Peserta_Didik() {
        return ID_Peserta_Didik;
    }

    public void setID_Peserta_Didik(UUID ID_Peserta_Didik) {
        this.ID_Peserta_Didik = ID_Peserta_Didik;
    }

    public UUID getID_RomBel() {
        return ID_RomBel;
    }

    public void setID_RomBel(UUID ID_RomBel) {
        this.ID_RomBel = ID_RomBel;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Anggota Rombongan belajar:");
        builder.append(ID_AngRombel);
        builder.append("ID Peserta Didik :");
        builder.append(ID_Peserta_Didik);
        builder.append("ID Rombongan Belajar :");
        builder.append(ID_RomBel);
        return builder.toString();
    }
}
