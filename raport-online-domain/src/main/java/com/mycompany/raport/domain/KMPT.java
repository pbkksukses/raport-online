package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="kmpt")
public class KMPT {
        @Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="id_kmpt")
	private UUID ID_KMPT;
        
        @Column(name="jns_kmpt")
        private String Jns_KMPT;
        
        @Column(name="Nomor_KMPT")
        private int Nomor_KMPT;
        
        @Column(name="des_kmpt")
        private String Des_KMPT;
        
        @Column(name="id_mapel_tambah",nullable = true)
        private UUID ID_Mapel_tambah;
        
        public KMPT(){};
        public KMPT(String Jns_KMPT,int Nomor_KMPT,String Des_KMPT){
            //this.setID_KMPT(ID_KMPT);
            this.setJns_KMPT(Jns_KMPT);
            this.setNomor_KMPT(Nomor_KMPT);
            this.setDes_KMPT(Des_KMPT);
            //this.setID_Mapel_tambah(null);
        };

    public UUID getID_KMPT() {
        return ID_KMPT;
    }

    public void setID_KMPT(UUID ID_KMPT) {
        this.ID_KMPT = ID_KMPT;
    }

    public String getJns_KMPT() {
        return Jns_KMPT;
    }

    public void setJns_KMPT(String Jns_KMPT) {
        this.Jns_KMPT = Jns_KMPT;
    }

    public int getNomor_KMPT() {
        return Nomor_KMPT;
    }

    public void setNomor_KMPT(int Nomor_KMPT) {
        this.Nomor_KMPT = Nomor_KMPT;
    }

    public String getDes_KMPT() {
        return Des_KMPT;
    }

    public void setDes_KMPT(String Des_KMPT) {
        this.Des_KMPT = Des_KMPT;
    }

    public UUID getID_Mapel_tambah() {
        return ID_Mapel_tambah;
    }

    public void setID_Mapel_tambah(UUID ID_Mapel_tambah) {
        this.ID_Mapel_tambah = ID_Mapel_tambah;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Kompetensi Mata Pelajaran Tambahan :");
        builder.append(ID_KMPT);
        builder.append("Jenis Kompetensi Mata Pelajaran Tambahan :");
        builder.append(Jns_KMPT);
        builder.append("Nomor Kompetensi Mata Pelajaran Tambahan");
        builder.append(Nomor_KMPT);
        builder.append("Deskripsi Kompetensi Mata Pelajaran Tambahan");
        builder.append(Des_KMPT);
        builder.append("ID_Pelajaran_Tambahan :");
        builder.append(ID_Mapel_tambah);
        return builder.toString();
    }
        
        
}
