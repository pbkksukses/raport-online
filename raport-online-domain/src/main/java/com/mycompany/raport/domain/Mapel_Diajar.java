package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="mapel_diajar")
public class Mapel_Diajar {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Mapel_Diajar")
    private UUID ID_mapel_Diajar;
    
    @Column(name = "Jenis_Mapel")
    private String Jenis_Mapel;
    
    @Column(name = "ID_KDM")
    private UUID ID_KMB;
    
    @Column(name = "ID_Mapel")
    private UUID ID_Mapel;
    
    @Column(name = "ID_Mapel_Tambah")
    private UUID ID_Mapel_Tambah;

    public Mapel_Diajar(String Jenis_Mapel,UUID ID_KMB,UUID ID_Mapel,UUID ID_Mapel_Tambah) {
        this.setJenis_Mapel(Jenis_Mapel);
        this.setID_KMB(ID_KMB);
        this.setID_Mapel(ID_Mapel);
        this.setID_Mapel_Tambah(ID_Mapel_Tambah);
        
    }

    public UUID getID_mapel_Diajar() {
        return ID_mapel_Diajar;
    }

    public void setID_mapel_Diajar(UUID ID_mapel_Diajar) {
        this.ID_mapel_Diajar = ID_mapel_Diajar;
    }

    public String getJenis_Mapel() {
        return Jenis_Mapel;
    }

    public void setJenis_Mapel(String Jenis_Mapel) {
        this.Jenis_Mapel = Jenis_Mapel;
    }

    public UUID getID_KMB() {
        return ID_KMB;
    }

    public void setID_KMB(UUID ID_KMB) {
        this.ID_KMB = ID_KMB;
    }

    public UUID getID_Mapel() {
        return ID_Mapel;
    }

    public void setID_Mapel(UUID ID_Mapel) {
        this.ID_Mapel = ID_Mapel;
    }

    public UUID getID_Mapel_Tambah() {
        return ID_Mapel_Tambah;
    }

    public void setID_Mapel_Tambah(UUID ID_Mapel_Tambah) {
        this.ID_Mapel_Tambah = ID_Mapel_Tambah;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Mapel Diajar :");
        builder.append(ID_mapel_Diajar);
        builder.append("Jenis Mapel :");
        builder.append(Jenis_Mapel);
        builder.append("ID KMB :");
        builder.append(ID_KMB);
        builder.append("ID Mapel :");
        builder.append(ID_Mapel);
        builder.append("ID Mapel Tambah :");
        builder.append(ID_Mapel_Tambah);
        return builder.toString();
    }
    
    
    
}
