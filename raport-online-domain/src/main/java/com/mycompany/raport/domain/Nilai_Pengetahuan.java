package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="nilai_pengetahuan")
public class Nilai_Pengetahuan {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Nilai_Pengetahuan")
    private UUID ID_Nilai_Pengetahuan;
    
    @Column(name = "Nilai_Harian")
    private Double Nilai_harian;
    
    @Column(name = "Nilai_UTS")
    private Double Nilai_UTS;
    
    @Column(name = "Nilai_UAS")
    private Double Nilai_UAS;
    
    @Column(name = "ID_Nilai")
    private UUID ID_Nilai;
    
    public Nilai_Pengetahuan(Double Nilai_Harian,Double Nilai_UTS,Double Nilai_UAS,UUID ID_Nilai){
        this.setNilai_harian(Nilai_harian);
        this.setNilai_UTS(Nilai_UTS);
        this.setNilai_UAS(Nilai_UAS);
        this.setID_Nilai(ID_Nilai);
    }

    public UUID getID_Niali_Pengetahuan() {
        return ID_Nilai_Pengetahuan;
    }

    public void setID_Niali_Pengetahuan(UUID ID_Niali_Pengetahuan) {
        this.ID_Nilai_Pengetahuan = ID_Niali_Pengetahuan;
    }

    public Double getNilai_harian() {
        return Nilai_harian;
    }

    public void setNilai_harian(Double Nilai_harian) {
        this.Nilai_harian = Nilai_harian;
    }

    public Double getNilai_UTS() {
        return Nilai_UTS;
    }

    public void setNilai_UTS(Double Nilai_UTS) {
        this.Nilai_UTS = Nilai_UTS;
    }

    public Double getNilai_UAS() {
        return Nilai_UAS;
    }

    public void setNilai_UAS(Double Nilai_UAS) {
        this.Nilai_UAS = Nilai_UAS;
    }

    public UUID getID_Nilai() {
        return ID_Nilai;
    }

    public void setID_Nilai(UUID ID_Nilai) {
        this.ID_Nilai = ID_Nilai;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID nilai Pengetahuan :");
        builder.append(ID_Nilai_Pengetahuan);
        builder.append("Nilai Harian :");
        builder.append(Nilai_harian);
        builder.append("Nilai UTS :");
        builder.append(Nilai_UTS);
        builder.append("Nilai UAS :");
        builder.append(Nilai_UAS);
        builder.append("ID Nilai :");
        builder.append(ID_Nilai);
        return builder.toString();
    }
}
