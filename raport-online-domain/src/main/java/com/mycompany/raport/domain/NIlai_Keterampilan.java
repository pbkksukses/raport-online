package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="nilai_keterampilan")
public class NIlai_Keterampilan {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Nilai_Keterampilan")
    private UUID ID_Nilai_Keterampilan;
    
    @Column(name = "Nilai_Praktek")
    private Double Nilai_Praktek;
    
    @Column(name = "Nilai_Proyek")
    private Double Nilai_Proyek;
    
    @Column(name = "NIlai_Portofolio")
    private Double Nilai_Portofolio;
    
    @Column(name = "ID_Nilai")
    private UUID ID_Nilai;
    
    public NIlai_Keterampilan(Double NIlai_Praktek,Double Nilai_Portofolio,Double Nilai_Proyek,UUID ID_Nilai){
        this.setNilai_Praktek(Nilai_Praktek);
        this.setNilai_Portofolio(Nilai_Portofolio);
        this.setNilai_Proyek(Nilai_Proyek);
        this.setID_Nilai(ID_Nilai);
    }

    public UUID getID_Nilai_Keterampilan() {
        return ID_Nilai_Keterampilan;
    }

    public void setID_Nilai_Keterampilan(UUID ID_Nilai_Keterampilan) {
        this.ID_Nilai_Keterampilan = ID_Nilai_Keterampilan;
    }

    public Double getNilai_Praktek() {
        return Nilai_Praktek;
    }

    public void setNilai_Praktek(Double Nilai_Praktek) {
        this.Nilai_Praktek = Nilai_Praktek;
    }

    public Double getNilai_Proyek() {
        return Nilai_Proyek;
    }

    public void setNilai_Proyek(Double Nilai_Proyek) {
        this.Nilai_Proyek = Nilai_Proyek;
    }

    public Double getNilai_Portofolio() {
        return Nilai_Portofolio;
    }

    public void setNilai_Portofolio(Double Nilai_Portofolio) {
        this.Nilai_Portofolio = Nilai_Portofolio;
    }

    public UUID getID_Nilai() {
        return ID_Nilai;
    }

    public void setID_Nilai(UUID ID_Nilai) {
        this.ID_Nilai = ID_Nilai;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Nilai Keterampilan :");
        builder.append(ID_Nilai_Keterampilan);
        builder.append("Nilai Praktek :");
        builder.append(Nilai_Praktek);
        builder.append("Nilai Proyek :");
        builder.append(Nilai_Proyek);
        builder.append("Nilai Portofolio :");
        builder.append(Nilai_Portofolio);
        builder.append("ID Nilai :");
        builder.append(ID_Nilai);
        return builder.toString();
    }
}
