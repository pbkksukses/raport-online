package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="thn_ajar")
public class Thn_Ajar {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Tahun_Ajaran")
    private UUID ID_tahun_Ajaran;
    
    @Column(name="Tahun_Tahun_Ajaran")
    private String Tahun_tahun_Ajaran;

    public Thn_Ajar(String Tahun_Tahun_Ajaran){
        this.setTahun_tahun_Ajaran(Tahun_tahun_Ajaran);
    }

    public UUID getID_tahun_Ajaran() {
        return ID_tahun_Ajaran;
    }

    public void setID_tahun_Ajaran(UUID ID_tahun_Ajaran) {
        this.ID_tahun_Ajaran = ID_tahun_Ajaran;
    }

    public String getTahun_tahun_Ajaran() {
        return Tahun_tahun_Ajaran;
    }

    public void setTahun_tahun_Ajaran(String Tahun_tahun_Ajaran) {
        this.Tahun_tahun_Ajaran = Tahun_tahun_Ajaran;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Tahun Ajar :");
        builder.append(ID_tahun_Ajaran);
        builder.append("Tahun Tahun Ajaran :");
        builder.append(Tahun_tahun_Ajaran);
        return builder.toString();
    }
    
    
}
