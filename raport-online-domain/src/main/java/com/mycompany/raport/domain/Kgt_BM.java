package com.mycompany.raport.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="kgt_bm")
public class Kgt_BM {
    @Id
    @GeneratedValue(generator="uuid2")
    @GenericGenerator(name="uuid2", strategy="uuid2")
    @Column(name="ID_Kgt_BM")
    private UUID ID_Kgt_BM;
    
    @Column(name = "ID_PDTK")
    private UUID ID_PDTK;
    
    @Column(name = "ID_RomBel")
    private UUID ID_RomBel;
    
    public Kgt_BM(UUID ID_PDTK,UUID ID_RomBel){
        this.setID_PDTK(ID_PDTK);
        this.setID_RomBel(ID_RomBel);
    }

    public UUID getID_Kgt_BM() {
        return ID_Kgt_BM;
    }

    public void setID_Kgt_BM(UUID ID_Kgt_BM) {
        this.ID_Kgt_BM = ID_Kgt_BM;
    }

    public UUID getID_PDTK() {
        return ID_PDTK;
    }

    public void setID_PDTK(UUID ID_PDTK) {
        this.ID_PDTK = ID_PDTK;
    }

    public UUID getID_RomBel() {
        return ID_RomBel;
    }

    public void setID_RomBel(UUID ID_RomBel) {
        this.ID_RomBel = ID_RomBel;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ID Kegiatan Belajar Mengajar :");
        builder.append(ID_Kgt_BM);
        builder.append("ID_PDTK :");
        builder.append(ID_PDTK);
        builder.append("ID Rombongan belajar :");
        builder.append(ID_RomBel);
        return builder.toString();
    }
}
